const orderid = require('order-id')('key')

module.exports = function codeGenerate(schema, code) {
    schema.pre('save', function (next) {
        const id = orderid.generate();
        
    
        this.code = `${code}-`+orderid.getTime(id);
        
        next()
    })
}