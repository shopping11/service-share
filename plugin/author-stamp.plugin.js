function stampAuthor(requestUser) {
    return {
        id: requestUser?._id || null,
        username: requestUser?.username || ''
    }
}


module.exports = function authorStampPlugin(schema) {
    schema.pre('save', function (next, options) {
   
        const doc = this
        const requestUser = doc._requestUser || options._requestUser
        this.updatedBy = stampAuthor(requestUser)
        if (this.isNew) {
        this.createdBy = stampAuthor(requestUser)
      }
        next() 
    })
    schema.pre('findOneAndUpdate', function (next) {
        const doc = this
        const requestUser = this.getOptions()._requestUser
        this.update({
            updatedBy: stampAuthor(requestUser)
          })
        next() 
    })
}