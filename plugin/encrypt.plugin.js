const Cryptr = require('cryptr')
cryptr = new Cryptr(process.env.SECRET_KEY)

module.exports = function encryptPlugin(schema, field) {
    schema.pre('save', function (next) {
        let doc = this
        for (const fieldToEncrypt of field) {
          const fieldValue = doc.get(fieldToEncrypt)
          if (!fieldValue) {
            continue
          }
            doc.set({ [fieldToEncrypt]: cryptr.encrypt(fieldValue) })
        }
        next() 
    });
    // schema.post('find', function (result) {
    //     for (let doc of result) {
    //         for (const fieldToEncrypt of field) {
    //             if (doc[fieldToEncrypt] === undefined ) {
    //                 const fieldValue = doc.get(fieldToEncrypt)
    //                 doc.set({ [fieldToEncrypt]: "test" })
    //                 // doc.set({ [fieldToEncrypt]: cryptr.decrypt(fieldValue) })
                    
    //             }
    //         }
    //     }
    // });
    schema.post('init', function (doc) {
        
        for (const fieldToEncrypt of field) {
            const fieldValue = doc.get(fieldToEncrypt)
      
            if (!fieldValue) {
              continue
            }
      
            const clearText = cryptr.decrypt(fieldValue) 
      
            doc[fieldToEncrypt] = clearText
          }
    });
  }