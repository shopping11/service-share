exports.APP_BAD_REQUEST = {
  INVALID_VALIDATE : { code: 400, message: 'invalid validate' },
  EMAIL_DUPLICATE: { code: 40001, message: 'this email already use' },
  USER_DUPLICATE: { code: 40002, message: 'this email already use' },
  LOGIN_FAILURE: { code: 40003, message: 'invalid user' },
  CATEGORY_NAME_DUPLICATE: { code: 40101, message: 'this category name already exist' },
  CATEGORY_NAME_CHILD_EXIST: { code: 40102, message: 'cant delete this id have child' },
  PRODUCT_NAME_DUPLICATE: { code: 40103, message: 'this product name already exist' },
  CART_ITEM_NOT_ENOUGHT: {code: 40204, message: 'this item not enough add to cart'}
}
  