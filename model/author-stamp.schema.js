const mongoose = require('mongoose')


exports.AuthorStampSchema = new mongoose.Schema({
    id: {
      type: mongoose.SchemaTypes.ObjectId,
      default: null
    },
    username: {
      type: String,
      default: ''
    }
  }, { _id: false })