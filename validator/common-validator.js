const {  validationResult } = require('express-validator');
const { APP_BAD_REQUEST } = require('../const/bad-request.const');
const { ErrorHandler, AppResponseError} = require('../middle-ware/response.middle-ware')
exports.IsRequestValidated = (req, res, next) => {
    const errors = validationResult(req);
  if (errors.array().length > 0) {
    throw new AppResponseError(400,APP_BAD_REQUEST.INVALID_VALIDATE,errors.array().map((err) => {
              return { [err.param]: err.msg }
      })
      )
    }
    next()
}