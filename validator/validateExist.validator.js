const mongoose = require("mongoose")

exports.ValidateExistError = (model, condition, appeEror) => {
  return mongoose.connection.models[model].find(condition).then((data) => {
    if (data.length > 0) {
      return Promise.reject(appeEror || `${Object.keys(condition)} ใช้ไปแล้ว`)
    }
  })
}

exports.ValidateExist = (model, condition) => {
  return mongoose.connection.models[model].find(condition).then((data) => {
    if (data.length <= 0) {
      return Promise.reject(appeEror || `${Object.keys(condition)} ไม่มีถูกต้อง`)
    }
  })
}