const jwt = require("jsonwebtoken")
const {ErrorHandler} = require('./response.middle-ware')

exports.requireSignin = (req, res, next) => {
  try {
    if (req.headers.authorization) {
      const token = req.headers.authorization.split(" ")[1];
      const user = jwt.verify(token, process.env.JWT_SECRET);
      req.user = user;
      } else {
      throw new ErrorHandler(400, 'Authorization required')
    }
    next();
  } catch(err) {
      throw new ErrorHandler(401, 'token expired')
    }
  }
