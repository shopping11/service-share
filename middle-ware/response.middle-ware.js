class ErrorHandler extends Error {
  constructor(statusCode, message) {
    super()
    this.statusCode = statusCode
    this.message = message
  }
}

class AppResponseError extends Error {
constructor(code,appError,data) {
  super()
  this.statusCode = code
  this.code = appError.code
  this.message = appError.message
  this.error = data
}
}

const responseHandle = (data, req, res, next) => {
if (data instanceof Error) {
  const response = tranformError(data?.response || data)

  res.status(response.status || response.statusCode || 500).send({
    code: response.code || response.statusCode || 500,
    message: response.message,
    data: response.error,
    path: req.path
  })
}
}

const tranformError = (data) => { 
  if (data?.data) { 
    return {
      status: data.status,
      ...data.data
    }
  }
  return data
}


const asyncHandler = fn => async (req, res, next) => {
let response
try {
  const data = await fn(req, res, next)
  if (res.headersSent) {
    return 
  }
  if (data?.docs) {
    response = paginationTransformer(data)
  } else {
    response = {data: data}
  }
  res.status(200).send({
    code: 200,
    message: "done",
    ...response
  })
} catch (err) {
  next(err)
}
}

const paginationTransformer =(data) => {
const { docs, ...filter } = data

return {
  data: docs.map(doc => doc),
  ...filter
}
}

module.exports = {
ErrorHandler,
responseHandle,
asyncHandler,
AppResponseError
}